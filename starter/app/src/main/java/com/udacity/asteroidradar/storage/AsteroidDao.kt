package com.udacity.asteroidradar.storage

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.udacity.asteroidradar.Asteroid

@Dao
interface AsteroidDao {

    @Query("SELECT * FROM asteroid order by closeApproachDate")
    suspend fun getAll(): List<Asteroid>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveList(list: List<Asteroid>)

    @Query("SELECT * FROM asteroid where closeApproachDate between :startDate and :endDate order by closeApproachDate")
    suspend fun getDataByDate(startDate: String, endDate: String): List<Asteroid>

}