package com.udacity.asteroidradar.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.udacity.asteroidradar.repo.MainRepo
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber

class RefreshDataWorker(
        context: Context,
        params: WorkerParameters
) :
        CoroutineWorker(context, params), KoinComponent {

    private val mainRepo: MainRepo by inject()

    companion object {
        const val NAME = "RefreshDataWorker"
    }

    override suspend fun doWork(): Result {
        return try {
            Timber.d("RefreshDataWorker")
            val resultRefresh = mainRepo.downloadTodayAsteroids()
            if (resultRefresh is com.udacity.asteroidradar.api.Result.Success)
                Result.success()
            else Result.failure()
        } catch (e: Exception) {
            Timber.e(e)
            Result.failure()
        }
    }
}