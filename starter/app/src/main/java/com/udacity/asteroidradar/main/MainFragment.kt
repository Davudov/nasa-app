package com.udacity.asteroidradar.main

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import com.udacity.asteroidradar.Asteroid
import com.udacity.asteroidradar.R
import com.udacity.asteroidradar.databinding.FragmentMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {
    private val viewModel: MainViewModel by viewModel()
    private lateinit var navController: NavController
    private lateinit var asteroidAdapter: AsteroidAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentMainBinding.inflate(inflater)
        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        navController = findNavController()

        asteroidAdapter = AsteroidAdapter(object : AsteroidClickLister {
            override fun onClick(asteroid: Asteroid) {
                navController.navigate(MainFragmentDirections.actionShowDetail(asteroid))
            }
        })


        binding.asteroidRecycler.apply {
            adapter = asteroidAdapter
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        viewModel.liveDataFeeds.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            asteroidAdapter.addData(it)
        })

        viewModel.liveDataImageUrl.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            Picasso.with(requireContext()).load(it.url).into(binding.activityMainImageOfTheDay)
            binding.activityMainImageOfTheDay.contentDescription = it.title
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getFeedWeeklyFromServer()
        viewModel.getImageUrl()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_overflow_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.show_saved_only -> {
                viewModel.getFeedSavedAsteroids()
            }
            R.id.show_today -> {
                viewModel.getFeedTodayFromServer()
            }
            R.id.show_weekly -> {
                viewModel.getFeedWeeklyFromServer()
            }
        }
        return true
    }
}
