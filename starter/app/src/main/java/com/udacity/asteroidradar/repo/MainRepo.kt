package com.udacity.asteroidradar.repo

import com.udacity.asteroidradar.Asteroid
import com.udacity.asteroidradar.api.*
import com.udacity.asteroidradar.storage.AsteroidDao
import com.udacity.asteroidradar.util.DateUtils
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import timber.log.Timber


class MainRepo(
        private val radarApiService: RadarApiService,
        private val dao: AsteroidDao
) {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO


    suspend fun getFeedByTime(startDate: String, endDate: String): Result<List<Asteroid>> {
        return withContext(ioDispatcher) {
            try {
                val result = radarApiService.getFeed(startDate, endDate, RadarApi.API_KEY).await()
                val resultList = parseAsteroidsJsonResult(JSONObject(result.string()))
                dao.saveList(resultList)
                Result.Success(resultList)
            } catch (e: Exception) {
                Timber.e(e)
                getFeedByTimeFromLocal(startDate, endDate)
                return@withContext Result.Error(e)
            }
        }
    }

    suspend fun getFeedToday(startDate: String, endDate: String): Result<List<Asteroid>> {
        return withContext(ioDispatcher) {
            try {
                val result = radarApiService.getFeed(startDate, endDate, RadarApi.API_KEY).await()
                val resultList = parseAsteroidsOneDayJsonResult(JSONObject(result.string()))
                dao.saveList(resultList)
                Result.Success(resultList)
            } catch (e: Exception) {
                Timber.e(e)
                return@withContext Result.Error(e)
            }
        }
    }

    suspend fun getTodayImageUrl(): Result<ResponseImage> {
        return withContext(ioDispatcher) {
            try {
                val result = radarApiService.getImage(RadarApi.API_KEY).await()
                Result.Success(result)

            } catch (e: Exception) {
                Timber.e(e)
                return@withContext Result.Error(e)
            }
        }
    }

    suspend fun downloadTodayAsteroids(): Result<List<Asteroid>> {
        return try {
            val result = getFeedToday(DateUtils.getTodayFormatted(), DateUtils.getTodayFormatted())
            if (result is Result.Success)
                dao.saveList(result.data)
            result
        } catch (e: java.lang.Exception) {
            Result.Error(e)
        }
    }

    suspend fun getFeedByTimeFromLocal(startDate: String, endDate: String): Result<List<Asteroid>> {
        return withContext(ioDispatcher) {
            try {
                val result = dao.getDataByDate(startDate, endDate)
                Result.Success(result)
            } catch (e: Exception) {
                Timber.e(e)
                return@withContext Result.Error(e)
            }
        }
    }

    suspend fun getFeedAllFromLocal(): Result<List<Asteroid>> {
        return withContext(ioDispatcher) {
            try {
                val result = dao.getAll()
                Result.Success(result)
            } catch (e: Exception) {
                Timber.e(e)
                return@withContext Result.Error(e)
            }
        }
    }
}
