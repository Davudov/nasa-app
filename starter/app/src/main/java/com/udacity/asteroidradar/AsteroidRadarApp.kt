package com.udacity.asteroidradar

import android.app.Application
import android.os.Build
import androidx.lifecycle.LifecycleObserver
import androidx.room.Room
import androidx.work.*
import com.udacity.asteroidradar.api.RadarApi
import com.udacity.asteroidradar.main.MainViewModel
import com.udacity.asteroidradar.repo.MainRepo
import com.udacity.asteroidradar.storage.AppDatabase
import com.udacity.asteroidradar.storage.AsteroidDao
import com.udacity.asteroidradar.work.RefreshDataWorker
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit

class AsteroidRadarApp : Application(), LifecycleObserver {


//    private lateinit var work: PeriodicWorkRequest

    companion object {
        lateinit var INSTANCE: AsteroidRadarApp
//        val DEFAULT_SHARED_PREFERENCES = "default"
    }


    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        initKoin()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        setupPersistWork()
    }

    private fun initKoin() {
        val moduleDefinition = module {

            viewModel {
                MainViewModel(get())
            }
            single {
                RadarApi.RETROFIT_SERVICE
            }

            single {
                MainRepo(get(), get())
            }

            single { createDatabase() }

            single {
                createAsteroidDao(get())
            }
        }
        startKoin {
            androidContext(this@AsteroidRadarApp)
            modules(listOf(moduleDefinition))
        }
    }

    private fun createDatabase(): AppDatabase {
        return Room.databaseBuilder(
                this@AsteroidRadarApp,
                AppDatabase::class.java,
                AppDatabase.name
        )
                .fallbackToDestructiveMigration() //File to re-create the database, otherwise this will delete all of the data in the
//                * database tables managed by Room.
                .build()

    }

    private fun createAsteroidDao(appDatabase: AppDatabase): AsteroidDao {
        return appDatabase.asteroidDao()
    }

//    private fun observeLifecycle() {
//        ProcessLifecycleOwner.get().lifecycle.addObserver(this@AsteroidRadarApp)
//    }

    private fun setupPersistWork() {
        val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.UNMETERED)
                .setRequiresBatteryNotLow(true)
                .setRequiresCharging(true)
                .apply {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        setRequiresDeviceIdle(true)
                    }
                }.build()


        val repeatingRequest = PeriodicWorkRequestBuilder<RefreshDataWorker>(
                1,
                TimeUnit.DAYS
        )
                .setConstraints(constraints)
                .build()

        WorkManager.getInstance().enqueueUniquePeriodicWork(
                RefreshDataWorker.NAME,
                ExistingPeriodicWorkPolicy.REPLACE,
                repeatingRequest
        )
    }
}