package com.udacity.asteroidradar.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.udacity.asteroidradar.Asteroid
import com.udacity.asteroidradar.api.ResponseImage
import com.udacity.asteroidradar.api.Result
import com.udacity.asteroidradar.repo.MainRepo
import com.udacity.asteroidradar.util.DateUtils
import kotlinx.coroutines.launch

class MainViewModel(private val mainRepo: MainRepo) : ViewModel() {

    private val _liveDataFeeds = MutableLiveData<List<Asteroid>>()

    val liveDataFeeds: LiveData<List<Asteroid>> = _liveDataFeeds

    private val _liveImageUrl = MutableLiveData<ResponseImage>()
    val liveDataImageUrl: LiveData<ResponseImage> = _liveImageUrl

    private fun getFeedByDateFromServer(startDate: String, endDate: String) {
        viewModelScope.launch {
            val result = mainRepo.getFeedByTime(startDate, endDate)
            if (result is Result.Success)
                _liveDataFeeds.postValue(result.data)
            else getFeedByDateFromLocal(startDate, endDate)
        }
    }

    private fun getFeedTodayFromServer(startDate: String, endDate: String) {
        viewModelScope.launch {
            val result = mainRepo.getFeedToday(startDate, endDate)
            if (result is Result.Success)
                _liveDataFeeds.postValue(result.data)
            else getFeedByDateFromLocal(startDate, endDate)
        }
    }

    private fun getFeedByDateFromLocal(startDate: String, endDate: String) {
        viewModelScope.launch {
            val result = mainRepo.getFeedByTimeFromLocal(startDate, endDate)
            if (result is Result.Success) {
                _liveDataFeeds.postValue(result.data)
            }
        }
    }

    fun getFeedWeeklyFromServer() {
        getFeedByDateFromServer(DateUtils.getTodayFormatted(), DateUtils.getNextSevenDayFormattedDate())
    }

    fun getFeedTodayFromServer() {
        getFeedTodayFromServer(DateUtils.getTodayFormatted(), DateUtils.getTodayFormatted())
    }

    fun getFeedSavedAsteroids() {
        viewModelScope.launch {
            val result = mainRepo.getFeedAllFromLocal()
            if (result is Result.Success) {
                _liveDataFeeds.postValue(result.data)
            }
        }
    }

    fun getImageUrl() {
        viewModelScope.launch {
            val result = mainRepo.getTodayImageUrl()
            if (result is Result.Success) {
                _liveImageUrl.postValue(result.data)
            }
        }
    }
}