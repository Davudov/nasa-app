package com.udacity.asteroidradar.util

import com.udacity.asteroidradar.Constants
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    fun getTodayFormatted(): String {
        val sdf = SimpleDateFormat(Constants.API_QUERY_DATE_FORMAT, Locale.getDefault())
        val todayFormatted = sdf.format(Date())
        return todayFormatted
    }


    fun getNextSevenDayFormattedDate(): String {
        var formattedDate = ""
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, Constants.DEFAULT_END_DATE_DAYS)
        val currentTime = calendar.time
        val dateFormat = SimpleDateFormat(Constants.API_QUERY_DATE_FORMAT, Locale.getDefault())
        formattedDate = dateFormat.format(currentTime)
        return formattedDate
    }
}