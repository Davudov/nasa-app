package com.udacity.asteroidradar.storage

import androidx.databinding.adapters.Converters
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.udacity.asteroidradar.Asteroid


@Database(entities = [Asteroid::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun asteroidDao(): AsteroidDao

    companion object {
        var name = "database"
    }
}
